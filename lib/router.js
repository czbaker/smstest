// Home route (test page)
Router.route("/", function() {
    this.render("home")
}, {
    name: "home",
    waitOn: function() {
        return Meteor.subscribe("people");
        return Meteor.subscribe("messages");
    }
});
