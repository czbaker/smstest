Meteor.publish("messages", function() {
    return Messages.find({});
});

Meteor.publish("people", function() {
    return People.find({});
});
