Meteor.methods({

    sendSMS: function(doc){

        var SMSClient = new Twilio('ACc359e42131ba3c4a12c4d1a0121923fb', '51f6597778591896613b51fcb2a8007e');
        SMSClient.sendMessage({
            to: doc.number,
            from: '+17404144550',
            body: doc.message
        }, function(err, response) {
            if (!err) {
                // Log stuff (Debug)
                console.log("Sent SMS From: " + response.from);
                console.log("Message Sent: " + response.body);
            }
        });

    },

    addPerson: function(doc) {

        theNum = doc.number;
        var personCheck = People.findOne({number: theNum});

        if (!personCheck) {
            People.insert(doc);
        } else {
            throw new Meteor.Error("person-exists", "This person already exists!");
        }

    }

});
