// POST route?
Router.route("/smsreply", {
    where: 'server'
}).post(function() {

    // Handle for the request
    var request = this.request.body;

    // Grab phone number from request object.
    var phone = request.From;

    // Get user object based on this phone number.
    var thePerson = People.findOne({phone: phone});
    var theName;

    // If no person is found with this number, just set the name equal to the number.
    if (!thePerson) {
        theName = phone;
    }

    // Store the following information:  Number, Name, Message.
    var messageDoc = {
        phone: phone,
        name: theName,
        message: request.Body
    };

    // Insert document into database
    Messages.insert(messageDoc);

});
