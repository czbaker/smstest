// Form Validation via Semantic UI
// Template.home.onRendered(function() {
//
//     $('.ui.form').form({
//         fields: {
//             name: 'empty',
//             phone: ['exactLength[10]', 'empty', 'integer'],
//             message: ['maxLength[160]', 'empty']
//         }
//     });
//
// });

Template.home.helpers({
    messages: function() {
        return Messages.find({});
    }
});


// Form Handler
Template.home.events({

    'submit #sms': function(event) {

        event.preventDefault();

        // Get form input
        var theName = $('.form').form('get value', 'name')
        var theNumber = "+1" + $('.form').form('get value', 'phone').replace(/[^\d]/g, "");
        var theMessage = $('.form').form('get value', 'message');

        console.log("Name: " + theName);
        console.log("Number: " + theNumber);
        console.log("Message: " + theMessage);

        var persondoc = {
            number: theNumber,
            name: theName
        };


        var SMSdoc = {
            name: theName,
            number: theNumber,
            message: theMessage
        }

        // Store Person
        Meteor.call('addPerson', persondoc, function(error) {
            if (error && error.error == "person-exists") {
                alert("This person already exists!");
            }
        });

        // Send SMS
        Meteor.call('sendSMS', SMSdoc);


    }

})
