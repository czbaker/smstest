People = new Mongo.Collection("people");

PeopleSchema = new SimpleSchema({
    name: {
        type: String
    },
    number: {
        type: String
    }
});

People.attachSchema(PeopleSchema);
