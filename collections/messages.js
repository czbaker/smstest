Messages = new Mongo.Collection("messages");

MessageSchema = new SimpleSchema({
    phone: {
        type: String
    },
    name: {
        type: String
    },
    message: {
        type: String
    }
});

Messages.attachSchema(MessageSchema);
Messages.attachBehaviour('timestampable', {
    createdBy: false,
    updatedBy: false
});
